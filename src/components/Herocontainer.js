import React from 'react'
import './Herocontainer.css' 

function Herocontainer() {
    return (
        <div
        className="hero-container"
        >
           <h1 style={{
               color: "white",
               marginTop:'2px',
               paddingTop:'15px'
           }}>Welcome</h1> 
           <div className="hero-btns">
              <h2>Millions of movies, TV shows and people to discover. Explore now.</h2>
           </div>
           <div style={{paddingTop:'40px'}}>
           <form className="d-flex">
            <input style={{paddingBottom:"15px"}} className="form-control me-4" type="search" placeholder="Search for a name of movie,TV or person" aria-label="Search"/>
            <div ><button style={{marginLeft:"-200%",paddingRight:"60%", paddingBottom:"15px",textAlign:"center", backgroundColor:"green", color:"white"}} className="btn btn-outline-success" type="submit">Search</button>
          </div></form>
           </div>

            
            
        </div>
    )
}

export default Herocontainer
