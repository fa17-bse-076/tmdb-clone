import React from 'react'
import "./Trendings.css"
import { useEffect , useState } from 'react'
import { Link } from 'react-router-dom'
const IMG_API='https://image.tmdb.org/t/p/w500'
const API_KEY ="https://api.themoviedb.org/3/trending/all/day?api_key=34cc5740c13722824a35ca7daa30fa40"

function Trendings() {
    const [trendings, setTrendings]= useState([])
  
    useEffect(()=>{
        
        fetch("https://api.themoviedb.org/3/trending/all/day?api_key=34cc5740c13722824a35ca7daa30fa40")
        .then((res)=> res.json())
        .then((data)=>{
            setTrendings(data.results)
            console.log(trendings)
        })  
    },[])
    return (<>{
        trendings.map((trend)=>(
            <Link to="/MovieDetails">
            <div
            className="trending-container"
            >
                
              <img style={{width:"100%"}} src={IMG_API+trend.poster_path} alt={trend.title}></img>
                <div className="trending-info" style={{fontSize:"12px", textDecorationLine:"none"}}>{trend.title} <span>{trend.vote_average}</span></div>
            
               
                
                
            </div>
            </Link>    ))
       
    }</>)
}

export default Trendings
