import React, { useEffect } from 'react'
import {Link } from 'react-router-dom'
import MovieDetails from './MovieDetails'
import "./Popular.css"
import { useState } from 'react'
const IMG_API='https://image.tmdb.org/t/p/w500'
const API_KEY ='https://api.themoviedb.org/3/discover/movie?api_key=34cc5740c13722824a35ca7daa30fa40'

function Popular() {
    const [movies, setMovies]= useState([])
  
    useEffect(()=>{
        
        fetch(API_KEY)
        .then((res)=> res.json())
        .then((data)=>{
            setMovies(data.results)
            console.log(movies)
        })  
    },[])
  
    return (
        
        
            // <Link to="/MovieDetails" >
            <>
            {
                movies.map((mov)=>(
                    <Link  key={mov.id} to="/MovieDetails">
                    <div
        className="popular-container"
        >
            <img style={{width:"100%"}} src={IMG_API+mov.poster_path} alt={mov.title}></img>
            <div className="movie-info">{mov.title} <span>{mov.vote_average}</span></div>
        </div>
        </Link>
                ))
             
        }
        </>
        )
}

export default Popular
