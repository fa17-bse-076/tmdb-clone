import './Home.css';
import Footer from "../Footer";
import React, { useState } from 'react';
import Popular from '../Popular';
import Trailers from '../Trailers';
import SecondFooter from '../SecondFooter';
import Trendings from '../Trendings';
import { useEffect} from 'react';
import {axios} from 'axios';
import { Link } from 'react-router-dom';
import Herocontainer from "../Herocontainer"
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
const API_KEY ='https://api.themoviedb.org/3/discover/movie?api_key=34cc5740c13722824a35ca7daa30fa40'
const TRENDING_API ="https://api.themoviedb.org/3/trending/all/day?api_key=34cc5740c13722824a35ca7daa30fa40"



function Home() {
    return(
      
    <>
    <div className="">
     <Herocontainer/>
   
    <h3 style={{
      marginTop:"40px",
      marginLeft:"20px"
    }}>What's Popular</h3>
    
 <div className="movie-container">
    <Popular />
)    </div>
   
<h3 style={{
      marginTop:"40px",
      marginLeft:"20px"
    }}>Trailers</h3>
 
    <div className="trailer-container">
    <Trailers />
     </div>

    <h3 style={{
      marginTop:"40px",
      marginLeft:"20px"
    }}>Trendings</h3>
    <div className="movie-container">
     
       <Trendings />
       
    
    </div>  
    <SecondFooter/>
    <Footer/>
    </div>
    </>
  )
  }

export default Home;
