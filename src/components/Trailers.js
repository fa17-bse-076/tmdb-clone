import React, { useEffect } from 'react'
import "./Trailers.css"
import ReactPlayer from 'react-player'
import ModalVideo from 'react-modal-video'
import { useState } from 'react'
import "../../node_modules/react-modal-video/scss/modal-video.scss"
import { Link } from 'react-router-dom'
const YOUTUBE_LINK ="https://www.youtube.com/watch?v="
const IMG_API='https://image.tmdb.org/t/p/w500'
function Trailers() {
    const [isOpen, setOpen] = useState(false)
    const [trailers, setTrailers]= useState([])
  
    useEffect(()=>{
        
        fetch('https://api.themoviedb.org/3/movie/848278?api_key=34cc5740c13722824a35ca7daa30fa40&append_to_response=videos')
        .then((res)=> res.json())
        .then((data)=>{
            setTrailers(data.results)
        })  
    },[])
    return (
            <div
        className="Trailers-container"
        >
           <React.Fragment>
			<ModalVideo channel='youtube' autoplay isOpen={isOpen} videoId="H9gpZqn-TtU" onClose={() => setOpen(false)} />

			<img src={IMG_API+"/6EdKBYkB1ssgGjc249ud1L55o8d.jpg"} className="btn-primary" onClick={()=> setOpen(true)} alt="asad"/>
           <Link to="/MovieDetails"> <h3 style={{color:"white",fontWeight:"bolder", textAlign:"center"}}>Juresic Park</h3></Link>
        </React.Fragment>
        {/* <ReactPlayer
           controls
           url="https://www.youtube.com/watch?v=dCDLPlZAoeY"
           />
           {
               console.log(YOUTUBE_LINK + data.key)
              // console.log(key)
           }
            */}
            
            
        </div>
    )
}

export default Trailers
