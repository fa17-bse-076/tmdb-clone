import React from 'react'
import "./SecondFooter.css"

function SecondFooter() {
    return (
       <div className="flex-container">
        <div
        className="hero-containers"
        >
           <h1 style={{
               color: "white",
               marginTop:'2px',
               paddingTop:'50px',
               paddingLeft:"7px"

           }}>Join today</h1> 
           <h3 style={{
               color: "white"}}>Get access to maintain your own custom personal lists, track what you've seen and search and filter for what to watch next—regardless if it's in theatres, on TV or available on popular streaming services like .</h3>
          <button className="button">Sign-up</button>
            
            
        </div>
        <div
        className="hero-containers"
        >
            <h5 style={{color:"white",
             marginTop:'2px',
             paddingTop:'100px',
                     }}>
            <ul>
                <li>Enjoy TMDB ad free</li>
                <li>Maintain a personal watchlist</li>
                <li>Filter by your subscribed streaming services and find something to watch</li>
                <li>Log the movies and TV shows you've seen</li>
                <li>Build custom lists</li>
                <li>Contribute to and improve our database</li>
            </ul>
            </h5>
            
            
        </div>
        
        </div>
       
    )
}

export default SecondFooter
